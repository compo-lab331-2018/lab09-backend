package se331.lab.rest.dao;

import se331.lab.rest.entity.Student;
import java.util.List;

public interface StudentDao {
    List<Student> getAllstudent();
    Student findById(Long id);
    Student saveStudent(Student student);
}
